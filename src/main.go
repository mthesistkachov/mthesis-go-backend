package main

import (
	"fmt"
	"github.com/go-kit/log/level"
	"github.com/gorilla/mux"
	stdprometheus "github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"mthesis/src/auth"
	"mthesis/src/config"
	"mthesis/src/db"
	"mthesis/src/log"
	"mthesis/src/myhttp"
	"mthesis/src/product"
	"mthesis/src/templating"
	"net/http"

	kitprometheus "github.com/go-kit/kit/metrics/prometheus"
)

func main() {
	logger, err := log.Init(true)
	if err != nil {
		fmt.Printf("Failed to initialize the logger. Reason: %v\n", err)
		return
	}

	level.Info(logger).Log("msg", "startup")

	conf := config.New()
	dbConn, err := db.New(conf)
	if err != nil {
		fmt.Printf("Failed to load db %v\n", err)
		return
	}

	authService, err := auth.NewKeycloakService(conf, logger)
	if err != nil {
		level.Error(logger).Log("msg", "failed to connect to keycloak, shutting down...")
		return
	}

	router := mux.NewRouter()

	fieldKeys := []string{"method"}

	productRepository := product.MakeRepository(dbConn)
	reviewRepository := product.MakeReviewRepository(dbConn)
	productServiceCore := product.MakeService(logger, productRepository, reviewRepository, authService)
	productService := product.MakeAuthService(productServiceCore, authService)
	productService = product.NewMetricsService(kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
		Namespace: "mthesis",
		Subsystem: "product_service",
		Name:      "request_count",
		Help:      "Number of requests received.",
	}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "mthesis",
			Subsystem: "product_service",
			Name:      "request_latency_microseconds",
			Help:      "Total duration of requests in microseconds.",
		}, fieldKeys),
		productService,
	)

	templateService := templating.MakeService(authService, productServiceCore)

	product.MakeHandler(router, productService)
	auth.MakeHandler(router, authService)
	//comms.MakeHandler(router, logger)
	templating.MakeHandler(router, templateService, logger)
	//email.MakeHandler(router, logger)
	router.Handle("/metrics", promhttp.Handler())
	http.Handle("/", router)

	requestIdInjectorMiddleware := myhttp.CtxIdMiddleware(http.DefaultServeMux)
	requestLogger := myhttp.RequestLoggerMiddleware(requestIdInjectorMiddleware, logger)
	authTokenMiddleware := myhttp.AuthTokenMiddleware(requestLogger)

	level.Info(logger).Log("msg", "starting API")
	err = http.ListenAndServe(":8080", authTokenMiddleware)
	if err != nil {
		level.Error(logger).Log("msg", "failed to start API", "err", err.Error())
		return
	}
}
