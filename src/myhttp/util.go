package myhttp

import (
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func ReturnHttpBadRoute() error {
	return &ResponseError{Msg:"bad route", Code: http.StatusNotFound}
}

func FindIdVariable(r *http.Request) (uint64, error) {
	vars := mux.Vars(r)
	idAsString, ok := vars["id"]
	if !ok {
		return 0, ReturnHttpBadRoute()
	}

	id, err := strconv.ParseUint(idAsString, 10, 32)
	if err != nil {
		return 0, ReturnHttpBadRoute()
	}

	return id, nil
}
