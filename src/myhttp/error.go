package myhttp

import (
	"encoding/json"
	"net/http"
)

type ResponseError struct {
	Msg  string
	Code int
}

type jsonRepresentation struct {
	Error string `json:"error"`
}

func (e *ResponseError) Error() string {
	return e.Msg
}

func (e *ResponseError) StatusCode() int {
	return e.Code
}

func (e *ResponseError) MarshalJSON() ([]byte, error) {
	return json.Marshal(jsonRepresentation{e.Msg})
}

func UnauthorizedError() error {
	return &ResponseError{
		Msg:  "You must be logged in to perform this operation",
		Code: http.StatusUnauthorized,
	}
}
