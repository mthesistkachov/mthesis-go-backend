package myhttp

import (
	"context"
	"github.com/go-kit/kit/log/level"
	"github.com/go-kit/log"
	"github.com/google/uuid"
	"net/http"
	"strings"
)

type contextKey string

const RequestIdKey = contextKey("request_id")
const AuthtokenKey = contextKey("auth_token")

func CtxIdMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Injects an uuid into the context to allow the grouping of log messages
		val := uuid.New()
		ctx := context.WithValue(r.Context(), RequestIdKey, val)
		r = r.WithContext(ctx)

		// Call the next handler, which can be another middleware in the chain, or the final handler.
		next.ServeHTTP(w, r)
	})
}

func RequestLoggerMiddleware(next http.Handler, logger log.Logger) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		level.Info(logger).Log("method", r.Method, "url", r.URL)
		next.ServeHTTP(w, r)
	})
}

func AuthTokenMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Injects the authorization token into the context
		header := r.Header.Get("Authorization")
		if strings.HasPrefix(header, "Bearer ") {
			token := strings.TrimPrefix(header, "Bearer ")
			ctx := context.WithValue(r.Context(), AuthtokenKey, token)
			r = r.WithContext(ctx)
		}

		// Call the next handler, which can be another middleware in the chain, or the final handler.
		next.ServeHTTP(w, r)
	})
}
