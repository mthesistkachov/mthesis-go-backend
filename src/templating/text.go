package templating

import (
	"bytes"
	"text/template"
)

const templateString = `Hello {{ .Fullname }},

{{if .Even}}Your secret random number is even!{{else}}Your secret random number is odd...{{end}}

You may be interested in the following products:
{{range .Products }} * {{ .Name }} for only {{ .Price }} $
{{end}}
`

type productData struct {
	Name  string
	Price float64
}

type templateData struct {
	Fullname     string
	RandomNumber int
	Even         bool
	Products     []productData
}

func buildFromTemplate(data templateData) (string, error) {
	t, err := template.New("example").Parse(templateString)
	if err != nil {
		return "", err
	}

	buf := bytes.NewBuffer([]byte{})
	err = t.Execute(buf, data)
	if err != nil {
		return "", err
	}

	str := buf.String()
	return str, nil
}
