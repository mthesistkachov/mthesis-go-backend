package templating

import (
	"context"
	"mthesis/src/auth"
	"mthesis/src/product"
	"time"
)

type Service struct {
	authService    auth.Service
	productService product.Service
}

func (s Service) buildText(ctx context.Context) (string, error) {
	user, err := s.authService.GetLoggedInUser(ctx)
	if err != nil {
		return "", err
	}

	products, err := s.productService.GetAll(ctx)
	if err != nil {
		return "", nil
	}

	fullname := user.Firstname + " " + user.Lastname
	randomNumer := time.Now().Second()
	even := randomNumer%2 == 0

	var trimmedProducts []productData
	for _, p := range products {
		trimmedProduct := productData{
			Name:  p.Name,
			Price: p.Price,
		}
		trimmedProducts = append(trimmedProducts, trimmedProduct)
	}

	tData := templateData{
		Fullname:     fullname,
		RandomNumber: randomNumer,
		Even:         even,
		Products:     trimmedProducts,
	}

	return buildFromTemplate(tData)
}

func MakeService(authService auth.Service, productService product.Service) Service {
	return Service{
		authService:    authService,
		productService: productService,
	}
}
