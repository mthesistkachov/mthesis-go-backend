package templating

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	"github.com/gorilla/mux"
	"mthesis/src/myhttp"
	"net/http"
)

func makeTemplateEndpoint(svc Service, logger log.Logger) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		text, err := svc.buildText(ctx)
		if err != nil {
			level.Warn(logger).Log("event", "templating", "err", err)
			return nil, &myhttp.ResponseError{
				Msg:  err.Error(),
				Code: 500,
			}
		}

		return templatingResponse{Text: text}, nil
	}
}

func decodeTemplatingRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return templatingRequest{}, nil
}

type templatingRequest struct {
}
type templatingResponse struct {
	Text string `json:"text"`
}

func MakeHandler(r *mux.Router, svc Service, logger log.Logger) {
	handler := kithttp.NewServer(
		makeTemplateEndpoint(svc, logger),
		decodeTemplatingRequest,
		kithttp.EncodeJSONResponse,
	)
	r.Handle("/templating", handler).Methods("GET")
}
