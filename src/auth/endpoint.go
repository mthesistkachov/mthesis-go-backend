package auth

import (
	"context"
	"github.com/go-kit/kit/endpoint"
)

type registerUserRequest struct {
	User User
}

func makeRegisterUserEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(registerUserRequest)
		user := req.User
		token, err := svc.CreateUser(ctx, user)
		return token, err
	}
}

type loginUserRequest struct {
	username string
	password string
}

func makeLoginUserEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(loginUserRequest)
		token, err := svc.IssueToken(ctx, req.username, req.password)
		return token, err
	}
}

type isUsernameAvailableRequest struct {
	username string
}

type isUsernameAvailableResponse struct {
	Available bool  `json:"available"`
	Err       error `json:"err,omitempty"`
}

func makeIsUsernameAvailableEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(isUsernameAvailableRequest)
		available, err := svc.IsUsernameAvailable(ctx, req.username)
		return isUsernameAvailableResponse{available, err}, err
	}
}

type userInformationRequest struct{}

type userInformationResponse struct {
	Id        string `json:"id,omitempty"`
	Username  string `json:"username,omitempty"`
	Firstname string `json:"firstname,omitempty"`
	Lastname  string `json:"lastname,omitempty"`
	Err       error  `json:"err,omitempty"`
}

func makeUserInformationEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		user, err := svc.GetLoggedInUser(ctx)
		if err != nil {
			return nil, err
		}

		return userInformationResponse{
			Id:        user.Id,
			Username:  user.Username,
			Firstname: user.Firstname,
			Lastname:  user.Lastname,
		}, nil
	}
}
