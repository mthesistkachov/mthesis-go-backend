package auth

import (
	"context"
	"github.com/Nerzal/gocloak/v10"
	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	"mthesis/src/config"
	"mthesis/src/myhttp"
	"time"
)

type Service interface {
	CreateUser(ctx context.Context, user User) (*gocloak.JWT, error)
	IssueToken(ctx context.Context, username string, password string) (*gocloak.JWT, error)
	IsTokenValid(ctx context.Context, token string) (bool, error)
	IsUsernameAvailable(ctx context.Context, username string) (bool, error)
	GetUserById(ctx context.Context, id string) (*User, error)
	GetLoggedInUser(ctx context.Context) (*User, error)
}

type User struct {
	Id        string
	Firstname string
	Lastname  string
	Username  string
	Password  string
}

type keycloakService struct {
	client        gocloak.GoCloak
	adminName     string
	adminPassword string
	clientId      string
	clientSecret  string
	realm         string
	logger        log.Logger
}

func (svc *keycloakService) getAdminToken(ctx context.Context) (string, error) {
	token, err := svc.client.LoginAdmin(ctx, svc.adminName, svc.adminPassword, "master")

	if err != nil {
		return "", err
	}

	return token.AccessToken, nil
}

func (svc *keycloakService) CreateUser(ctx context.Context, user User) (*gocloak.JWT, error) {
	level.Info(svc.logger).Log("msg", "creating user", "name", user.Username)
	gocloakUser := gocloak.User{
		FirstName: gocloak.StringP(user.Firstname),
		LastName:  gocloak.StringP(user.Lastname),
		Enabled:   gocloak.BoolP(true),
		Username:  gocloak.StringP(user.Username),
	}

	adminToken, err := svc.getAdminToken(ctx)
	if err != nil {
		return nil, err
	}

	id, err := svc.client.CreateUser(ctx, adminToken, svc.realm, gocloakUser)
	if err != nil {
		//level.Error(svc.logger).Log("msg", "failed to create user", "name", user.Username)
		return nil, err
	}

	err = svc.client.SetPassword(ctx, adminToken, id, svc.realm, user.Password, false)
	if err != nil {
		//level.Error(svc.logger).Log("msg", "failed to set password", "name", user.Username)
		return nil, err
	}

	return svc.IssueToken(ctx, user.Username, user.Password)
}

func (svc *keycloakService) IssueToken(ctx context.Context, username string, password string) (*gocloak.JWT, error) {
	token, err := svc.client.Login(ctx, svc.clientId, svc.clientSecret, svc.realm, username, password)

	if err != nil {
		return nil, &myhttp.ResponseError{
			Msg:  "login failed",
			Code: 400,
		}
	}

	return token, nil
}

func (svc *keycloakService) IsTokenValid(ctx context.Context, token string) (bool, error) {
	rptResult, err := svc.client.RetrospectToken(ctx, token, svc.clientId, svc.clientSecret, svc.realm)
	if err != nil {
		return false, err
	}

	return rptResult.Active != nil && *rptResult.Active, nil
}

func (svc *keycloakService) IsUsernameAvailable(ctx context.Context, username string) (bool, error) {
	params := gocloak.GetUsersParams{
		Username: gocloak.StringP(username),
	}

	adminToken, err := svc.getAdminToken(ctx)
	if err != nil {
		return false, err
	}

	users, err := svc.client.GetUsers(ctx, adminToken, svc.realm, params)
	if err != nil {
		return false, err
	}

	return len(users) == 0, nil
}

func (svc *keycloakService) GetLoggedInUser(ctx context.Context) (*User, error) {
	token := ctx.Value(myhttp.AuthtokenKey)
	if token == nil {
		return nil, myhttp.UnauthorizedError()
	}

	tokenAsString := token.(string)
	if tokenAsString == "" {
		return nil, myhttp.UnauthorizedError()
	}

	valid, err := svc.IsTokenValid(ctx, tokenAsString)
	if err != nil || !valid {
		return nil, myhttp.UnauthorizedError()
	}

	_, claims, err := svc.client.DecodeAccessToken(ctx, tokenAsString, svc.realm)
	if err != nil {
		return nil, myhttp.UnauthorizedError()
	}

	userId := (*claims)["sub"].(string)
	return svc.GetUserById(ctx, userId)
}

func (svc *keycloakService) GetUserById(ctx context.Context, id string) (*User, error) {
	adminToken, err := svc.getAdminToken(ctx)
	if err != nil {
		return nil, err
	}

	userKeycloak, err := svc.client.GetUserByID(ctx, adminToken, svc.realm, id)
	if err != nil {
		return nil, err
	}

	user := &User{
		Id:        id,
		Firstname: *userKeycloak.FirstName,
		Lastname:  *userKeycloak.LastName,
		Username:  *userKeycloak.Username,
	}

	return user, nil
}

func NewKeycloakService(conf *config.Config, logger log.Logger) (Service, error) {
	user := conf.KeycloakUser
	pass := conf.KeycloakPassword
	url := conf.KeycloakUrl
	ctx := context.Background()

	tries := 0
	for {
		level.Info(logger).Log("msg", "logging in to keycloak", "conn", url)
		client := gocloak.NewClient(url)

		service := &keycloakService{
			adminName:     user,
			adminPassword: pass,
			client:        client,
			clientId:      conf.KeycloakClient,
			clientSecret:  conf.KeycloakClientSecret,
			realm:         conf.KeycloakRealm,
			logger:        logger,
		}

		_, err := service.getAdminToken(ctx)
		if err == nil {
			level.Info(logger).Log("msg", "successfully logged into keycloak")
			return service, nil
		}

		level.Error(logger).Log("msg", "failed to log in into keycloak, retrying...", "err", err.Error())
		tries++

		if tries > 20 {
			level.Error(logger).Log("msg", "max amount of retries reached", "err", err.Error())
			return nil, err
		}

		<-time.After(5 * time.Second)
	}

}
