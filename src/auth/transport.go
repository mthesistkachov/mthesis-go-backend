package auth

import (
	"context"
	"encoding/json"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"net/http"
)

func decodeRegisterUserRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var body struct {
		Firstname string `json:"firstname"`
		Lastname  string `json:"lastname"`
		Username  string `json:"username"`
		Password  string `json:"password"`
	}

	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		return nil, err
	}

	user := User{
		Firstname: body.Firstname,
		Lastname:  body.Lastname,
		Username:  body.Username,
		Password:  body.Password,
	}
	return registerUserRequest{User: user}, nil
}

func decodeLoginUserRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var body struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		return nil, err
	}

	return loginUserRequest{
		username: body.Username,
		password: body.Password,
	}, nil
}

func decodeIsUsernameAvailableRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var body struct {
		Username string `json:"username"`
	}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		return nil, err
	}

	return isUsernameAvailableRequest{
		username: body.Username,
	}, nil
}

func decodeGetLoggedInUserRequest(_ context.Context, _ *http.Request) (interface{}, error) {
	return userInformationRequest{}, nil
}

func MakeHandler(r *mux.Router, service Service) {
	registerUserHandler := kithttp.NewServer(
		makeRegisterUserEndpoint(service),
		decodeRegisterUserRequest,
		kithttp.EncodeJSONResponse,
	)

	loginUserHandler := kithttp.NewServer(
		makeLoginUserEndpoint(service),
		decodeLoginUserRequest,
		kithttp.EncodeJSONResponse,
	)

	usernameAvailableHandler := kithttp.NewServer(
		makeIsUsernameAvailableEndpoint(service),
		decodeIsUsernameAvailableRequest,
		kithttp.EncodeJSONResponse,
	)

	getLoggedInUserRequestHandler := kithttp.NewServer(
		makeUserInformationEndpoint(service),
		decodeGetLoggedInUserRequest,
		kithttp.EncodeJSONResponse,
	)

	r.Handle("/auth/register", registerUserHandler).Methods("POST")
	r.Handle("/auth/login", loginUserHandler).Methods("POST")
	r.Handle("/auth/usernameavailability", usernameAvailableHandler).Methods("PUT")
	r.Handle("/auth/me", getLoggedInUserRequestHandler).Methods("GET")

}
