package comms

import (
	"context"
	"errors"
	"github.com/go-kit/log"
	"github.com/segmentio/kafka-go"
)

type kafkaService struct {
	reader *kafka.Reader
	writer *kafka.Writer
	logger log.Logger
}

func (s *kafkaService) readMessage() (string, error) {
	m, err := s.reader.ReadMessage(context.Background())
	if err != nil {
		return "", err
	}

	return string(m.Value), nil
}

func (s *kafkaService) writeMessage(message string) error {
	err := s.writer.WriteMessages(context.Background(),
		kafka.Message{
			Key:   []byte("message"),
			Value: []byte(message),
		},
	)

	if err != nil {
		switch err.(type) {
		case kafka.WriteErrors:
			err1 := err.(kafka.WriteErrors)
			msg := ""
			for _, v := range err1 {
				if v != nil {
					msg += v.Error() + "|"
				}
				return errors.New(msg)
			}
		default:
			return err
		}
	}

	return nil
}

func (s *kafkaService) Close() {
	s.reader.Close()
	s.writer.Close()
}

func MakeKafkaService(logger log.Logger) *kafkaService {
	kafkaHost := "kafka:9092"

	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers: []string{kafkaHost},
		Topic:   "echo-response",
		GroupID: "consumer-group",
	})

	w := &kafka.Writer{
		Addr:                   kafka.TCP(kafkaHost),
		Topic:                  "echo-request",
		AllowAutoTopicCreation: true,
	}

	return &kafkaService{
		reader: r,
		writer: w,
		logger: logger,
	}
}
