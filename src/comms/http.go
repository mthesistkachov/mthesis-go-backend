package comms

import (
	"encoding/json"
	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	"github.com/gorilla/mux"
	"net/http"
)

type body struct {
	Message string `json:"message"`
}

func createEchoHandler(svc *kafkaService) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		logger := svc.logger

		var requestBody *body
		if err := json.NewDecoder(r.Body).Decode(&requestBody); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		message := requestBody.Message

		err := svc.writeMessage(message)
		if err != nil {
			level.Warn(logger).Log("action", "echo_write", "err", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		level.Debug(logger).Log("action", "echo_write", "status", "successful")

		responseMessage, err := svc.readMessage()
		if err != nil {
			level.Warn(logger).Log("action", "echo_read", "err", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		level.Debug(logger).Log("action", "echo_read", "status", "successful")

		responseBody := body{
			Message: responseMessage,
		}

		j, err := json.Marshal(responseBody)
		if err != nil {
			level.Warn(logger).Log("action", "echo_json_marshal", "err", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		_, err = w.Write(j)
		if err != nil {
			level.Warn(logger).Log("action", "echo_http_write", "err", err)
			return
		}
	}

}

func MakeHandler(r *mux.Router, logger log.Logger) {
	svc := MakeKafkaService(logger)
	r.HandleFunc("/comms/echo", createEchoHandler(svc)).Methods("POST")
}
