package email

import (
	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	"github.com/gorilla/mux"
	"net/http"
)

func createHandler(logger log.Logger) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		err := sendTestMail()
		if err != nil {
			w.WriteHeader(500)
			level.Warn(logger).Log("event", "email", "err", err)
			return
		}

		level.Debug(logger).Log("event", "email", "status", "success")
	}
}

func MakeHandler(r *mux.Router, logger log.Logger) {
	r.HandleFunc("/testemail", createHandler(logger)).Methods("POST")
}
