package email

import "gopkg.in/gomail.v2"

const smtpServer = ""
const smtpPort = 0
const username = ""
const password = ""

const sender = ""
const receiver = ""

func sendTestMail() error {
	mail := gomail.NewMessage()

	mail.SetHeader("From", sender)
	mail.SetHeader("To", receiver)

	mail.SetHeader("Subject", "Test Email")
	mail.SetBody("text/plain", "This is just a test email")
	mail.Attach("email/attachment.txt")

	dialer := gomail.NewDialer(smtpServer, smtpPort, username, password)

	err := dialer.DialAndSend(mail)
	if err != nil {
		return err
	}

	return nil
}
