package product

import (
	"context"
	"fmt"
	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	"mthesis/src/auth"
	"mthesis/src/myhttp"
	"net/http"
)

const rid = myhttp.RequestIdKey

type service struct {
	logger            log.Logger
	productRepository Repository
	reviewRepository  ReviewRepository
	authService       auth.Service
}

func MakeService(
	logger log.Logger,
	productRepository Repository,
	reviewRepository ReviewRepository,
	authService auth.Service,
) Service {
	return &service{
		logger:            logger,
		productRepository: productRepository,
		reviewRepository:  reviewRepository,
		authService:       authService,
	}
}

func (svc *service) GetProductById(ctx context.Context, id ProductId) (*Product, error) {
	level.Info(svc.logger).Log(rid, ctx.Value(rid), "msg", "getting single product.", "id", id)
	product, err := svc.productRepository.GetById(id)

	if err != nil {
		level.Warn(svc.logger).Log("msg", "failed to find product", "id", id, "err", err.Error())
		return nil, &myhttp.ResponseError{
			Msg:  "product not found",
			Code: http.StatusNotFound,
		}
	}

	return product, nil
}

func (svc *service) GetAll(ctx context.Context) ([]*Product, error) {
	level.Info(svc.logger).Log(rid, ctx.Value(rid), "msg", "getting all products")
	products, err := svc.productRepository.GetAll()

	if err != nil {
		level.Warn(svc.logger).Log("msg", "failed to fetch all products", "err", err.Error())
		return nil, err
	}

	return products, nil
}

func (svc *service) CreateProduct(ctx context.Context, product *Product) (ProductId, error) {
	level.Info(svc.logger).Log(rid, ctx.Value(rid), "msg", "creating new product", "product", fmt.Sprintf("%#v", product))
	id, err := svc.productRepository.CreateProduct(product)
	if err != nil {
		level.Warn(svc.logger).Log("msg", "failed to create new product", "err", err.Error())
		return 0, err
	}
	return id, nil
}

func (svc *service) UpdateProduct(ctx context.Context, product *Product) error {
	level.Info(svc.logger).Log(rid, ctx.Value(rid), "msg", "updating product", "product", fmt.Sprintf("%#v", product))
	err := svc.productRepository.UpdateProduct(product)

	if err != nil {
		level.Warn(svc.logger).Log("msg", "failed to update product", "err", err.Error())
	}

	return err
}

func (svc *service) DeleteProduct(ctx context.Context, id ProductId) error {
	level.Info(svc.logger).Log(rid, ctx.Value(rid), "msg", "deleting product", "id", id)
	err := svc.productRepository.DeleteProduct(id)

	if err != nil {
		level.Warn(svc.logger).Log("msg", "failed to update product", "err", err.Error())
	}

	return err
}

func (svc *service) GetReviews(ctx context.Context, id ProductId) ([]*ReviewDTO, error) {
	reviews, err := svc.reviewRepository.GetByProductId(id)

	if err != nil {
		level.Warn(svc.logger).Log("msg", "failed to get reviews", "err", err.Error())
		return nil, err
	}

	reviewDTOs := make([]*ReviewDTO, 0, len(reviews))

	for _, review := range reviews {
		reviewer, err := svc.authService.GetUserById(ctx, review.UserId)

		if err != nil {
			level.Warn(svc.logger).Log("msg", "failed to find reviewer", "id", review.UserId)
			continue
		}

		reviewDTOs = append(reviewDTOs, &ReviewDTO{
			Id:         review.Id,
			ProductId:  review.ProductId,
			Title:      review.Title,
			Content:    review.Content,
			Rating:     review.Rating,
			ReviewerId: review.UserId,
			Reviewer:   reviewer.Firstname + " " + reviewer.Lastname,
		})
	}

	return reviewDTOs, nil
}

func (svc *service) CreateReview(ctx context.Context, productId ProductId, review *ReviewCreationDTO) (ReviewId, error) {
	user := ctx.Value(userContextKey).(*auth.User)

	reviewEntity := &Review{
		Title:     review.Title,
		Content:   review.Content,
		Rating:    review.Rating,
		ProductId: productId,
		UserId:    user.Id,
	}

	id, err := svc.reviewRepository.CreateReview(reviewEntity)

	if err != nil {
		level.Warn(svc.logger).Log("msg", "failed to create new review", "err", err.Error())
		return 0, err
	}

	return id, nil
}
