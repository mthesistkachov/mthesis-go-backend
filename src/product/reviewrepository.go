package product

import "gorm.io/gorm"

type reviewRepository struct {
	db *gorm.DB
}

func MakeReviewRepository(db *gorm.DB) ReviewRepository {
	return &reviewRepository{db: db}
}

func (r *reviewRepository) GetByProductId(id ProductId) ([]*Review, error) {
	var reviews []*Review
	err := r.db.Where("product_id = ?", id).Find(&reviews).Error

	if err != nil {
		return nil, err
	}

	return reviews, nil
}

func (r *reviewRepository) CreateReview(review *Review) (ReviewId, error) {
	err := r.db.Omit("id").Create(review).Error
	return review.Id, err
}
