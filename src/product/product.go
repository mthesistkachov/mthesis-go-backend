package product

type ProductId uint64

type Product struct {
	//gorm.Model
	Id          ProductId `gorm:"primarykey" json:"id"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
	Price       float64   `json:"price"` // TODO fixed scale
	IsAvailable bool      `json:"available"`
}
