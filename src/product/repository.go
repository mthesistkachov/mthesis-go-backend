package product

import (
	"gorm.io/gorm"
)

type repository struct {
	db *gorm.DB
}

func MakeRepository(db *gorm.DB) Repository {
	return &repository{db: db}
}

func (r *repository) GetById(id ProductId) (*Product, error) {
	var product *Product
	err := r.db.First(&product, id).Error

	if err != nil {
		return nil, err
	}

	return product, nil
}

func (r *repository) GetAll() ([]*Product, error) {
	var products []*Product
	err := r.db.Find(&products).Error

	if err != nil {
		return nil, err
	}

	return products, nil
}

func (r *repository) CreateProduct(product *Product) (ProductId, error) {
	err := r.db.Omit("id").Create(product).Error
	return product.Id, err
}

func (r *repository) UpdateProduct(product *Product) error {
	return r.db.Save(product).Error
}

func (r *repository) DeleteProduct(id ProductId) error {
	return r.db.Delete(&Product{}, id).Error
}
