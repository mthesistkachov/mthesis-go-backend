package product

type ReviewId uint64

type Review struct {
	Id        ReviewId `gorm:"primarykey"`
	Title     string
	ProductId ProductId
	Content   string
	Rating    int
	UserId    string
}

type ReviewDTO struct {
	Id         ReviewId  `json:"id"`
	ProductId  ProductId `json:"product_id"`
	Title      string    `json:"title"`
	Content    string    `json:"content"`
	Rating     int       `json:"rating"`
	ReviewerId string    `json:"reviewer_id"`
	Reviewer   string    `json:"reviewer"`
}

type ReviewCreationDTO struct {
	Title   string `json:"title"`
	Content string `json:"content"`
	Rating  int    `json:"rating"`
}
