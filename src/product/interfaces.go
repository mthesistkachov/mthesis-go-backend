package product

import "context"

type Repository interface {
	GetById(ProductId) (*Product, error)
	GetAll() ([]*Product, error)
	CreateProduct(*Product) (ProductId, error)
	UpdateProduct(*Product) error
	DeleteProduct(ProductId) error
}

type ReviewRepository interface {
	GetByProductId(ProductId) ([]*Review, error)
	CreateReview(*Review) (ReviewId, error)
}

type Service interface {
	GetProductById(context.Context, ProductId) (*Product, error)
	GetAll(context.Context) ([]*Product, error)
	CreateProduct(context.Context, *Product) (ProductId, error)
	UpdateProduct(context.Context, *Product) error
	DeleteProduct(context.Context, ProductId) error
	GetReviews(ctx context.Context, id ProductId) ([]*ReviewDTO, error)
	CreateReview(ctx context.Context, id ProductId, review *ReviewCreationDTO) (ReviewId, error)
}
