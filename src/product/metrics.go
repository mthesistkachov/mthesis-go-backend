package product

import (
	"context"
	"github.com/go-kit/kit/metrics"
	"time"
)

type metricsService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	service        Service
}

func NewMetricsService(counter metrics.Counter, latency metrics.Histogram, s Service) Service {
	return &metricsService{
		requestCount:   counter,
		requestLatency: latency,
		service:        s,
	}
}

func (s *metricsService) measure(begin time.Time, action string) {
	s.requestCount.With("method", action).Add(1)
	s.requestLatency.With("method", action).Observe(time.Since(begin).Seconds())
}

func (s *metricsService) GetProductById(ctx context.Context, id ProductId) (*Product, error) {
	defer s.measure(time.Now(), "get_single_product")
	return s.service.GetProductById(ctx, id)
}

func (s *metricsService) GetAll(ctx context.Context) ([]*Product, error) {
	defer s.measure(time.Now(), "get_all_products")
	return s.service.GetAll(ctx)
}

func (s *metricsService) CreateProduct(ctx context.Context, product *Product) (ProductId, error) {
	defer s.measure(time.Now(), "create_product")
	return s.service.CreateProduct(ctx, product)
}

func (s *metricsService) UpdateProduct(ctx context.Context, product *Product) error {
	defer s.measure(time.Now(), "update_product")
	return s.service.UpdateProduct(ctx, product)
}

func (s *metricsService) DeleteProduct(ctx context.Context, id ProductId) error {
	defer s.measure(time.Now(), "delete_product")
	return s.service.DeleteProduct(ctx, id)
}

func (s *metricsService) GetReviews(ctx context.Context, id ProductId) ([]*ReviewDTO, error) {
	defer s.measure(time.Now(), "get_reviews")
	return s.service.GetReviews(ctx, id)
}

func (s *metricsService) CreateReview(ctx context.Context, id ProductId, review *ReviewCreationDTO) (ReviewId, error) {
	defer s.measure(time.Now(), "create_review")
	return s.service.CreateReview(ctx, id, review)
}
