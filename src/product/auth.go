package product

import (
	"context"
	"mthesis/src/auth"
)

type contextKey string

const userContextKey = contextKey("user")

type productAuthService struct {
	authService    auth.Service
	productService Service
}

func MakeAuthService(productService Service, authService auth.Service) Service {
	return productAuthService{
		authService:    authService,
		productService: productService,
	}
}

func (svc productAuthService) GetProductById(ctx context.Context, id ProductId) (*Product, error) {
	return svc.productService.GetProductById(ctx, id)
}

func (svc productAuthService) GetAll(ctx context.Context) ([]*Product, error) {
	return svc.productService.GetAll(ctx)
}

func (svc productAuthService) CreateProduct(ctx context.Context, product *Product) (ProductId, error) {
	return svc.productService.CreateProduct(ctx, product)
}

func (svc productAuthService) UpdateProduct(ctx context.Context, product *Product) error {
	return svc.productService.UpdateProduct(ctx, product)
}

func (svc productAuthService) DeleteProduct(ctx context.Context, id ProductId) error {
	return svc.productService.DeleteProduct(ctx, id)
}

func (svc productAuthService) GetReviews(ctx context.Context, id ProductId) ([]*ReviewDTO, error) {
	// No auth required, just call the next service/middleware
	return svc.productService.GetReviews(ctx, id)
}

func (svc productAuthService) CreateReview(ctx context.Context, id ProductId, review *ReviewCreationDTO) (ReviewId, error) {
	user, err := svc.authService.GetLoggedInUser(ctx)
	if err != nil {
		return 0, err
	}

	// userContextKey is a constant
	ctx = context.WithValue(ctx, userContextKey, user)
	return svc.productService.CreateReview(ctx, id, review)
}
