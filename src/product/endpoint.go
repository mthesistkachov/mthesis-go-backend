package product

import (
	"context"
	"github.com/go-kit/kit/endpoint"
)

type getProductByIdRequest struct {
	Id ProductId
}

type getProductByIdResponse struct {
	Data *Product `json:"data,omitempty"`
	Err  error    `json:"err,omitempty"`
}

func makeGetProductByIdEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getProductByIdRequest)
		product, err := svc.GetProductById(ctx, req.Id)
		return getProductByIdResponse{product, err}, err
	}
}

type getAllProductsRequest struct{}

type getAllProductsResponse struct {
	Data []*Product `json:"data,omitempty"`
	Err  error      `json:"err,omitempty"`
}

func makeGetAllProductsEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		products, err := svc.GetAll(ctx)
		return getAllProductsResponse{products, err}, err
	}
}

type createProductRequest struct {
	Data *Product `json:"data"`
}

type createProductResponse struct {
	Id  ProductId `json:"id,omitempty"`
	Err error     `json:"error,omitempty"`
}

func makeCreateProductEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(createProductRequest)
		id, err := svc.CreateProduct(ctx, req.Data)
		return createProductResponse{id, err}, err
	}
}

type updateProductRequest struct {
	Data *Product `json:"data"`
}

type updateProductResponse struct {
	Err error `json:"error,omitempty"`
}

func makeUpdateProductEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateProductRequest)
		err := svc.UpdateProduct(ctx, req.Data)
		return updateProductResponse{err}, err
	}
}

type deleteProductRequest struct {
	Id ProductId
}

type deleteProductResponse struct {
	Err error `json:"error,omitempty"`
}

func makeDeleteProductEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(deleteProductRequest)
		err := svc.DeleteProduct(ctx, req.Id)
		return deleteProductResponse{err}, err
	}
}

type getReviewsRequest struct {
	Id ProductId
}

type getReviewsResponse struct {
	Data []*ReviewDTO `json:"data"`
	Err  error        `json:"err,omitempty"`
}

func makeGetReviewsEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getReviewsRequest)
		reviews, err := svc.GetReviews(ctx, req.Id)
		return getReviewsResponse{reviews, err}, err
	}
}

type createReviewRequest struct {
	ProductId ProductId
	Review    *ReviewCreationDTO
}

type createReviewResponse struct {
	Id  ReviewId `json:"id,omitempty"`
	Err error    `json:"error,omitempty"`
}

func makeCreateReviewEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(createReviewRequest)
		id, err := svc.CreateReview(ctx, req.ProductId, req.Review)
		return createReviewResponse{id, err}, err
	}
}
