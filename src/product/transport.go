package product

import (
	"context"
	"encoding/json"
	"mthesis/src/myhttp"
	"net/http"
	"strconv"

	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
)

func decodeGetProductByIdRequest(_ context.Context, r *http.Request) (interface{}, error) {
	vars := mux.Vars(r)
	idAsString, ok := vars["id"]
	if !ok {
		return nil, myhttp.ReturnHttpBadRoute()
	}

	id, err := strconv.ParseUint(idAsString, 10, 32)
	if err != nil {
		return nil, myhttp.ReturnHttpBadRoute()
	}

	var request getProductByIdRequest
	request.Id = ProductId(id)
	return request, nil
}

func decodeGetAllProducts(_ context.Context, _ *http.Request) (interface{}, error) {
	return getAllProductsRequest{}, nil
}

func decodeCreateProductRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var product *Product
	if err := json.NewDecoder(r.Body).Decode(&product); err != nil {
		return nil, err
	}

	return createProductRequest{Data: product}, nil
}

func decodeUpdateProductRequest(_ context.Context, r *http.Request) (interface{}, error) {
	id, err := myhttp.FindIdVariable(r)
	if err != nil {
		return nil, err
	}

	var product *Product
	if err := json.NewDecoder(r.Body).Decode(&product); err != nil {
		return nil, err
	}
	product.Id = ProductId(id)

	return updateProductRequest{Data: product}, nil
}

func decodeDeleteProductRequest(_ context.Context, r *http.Request) (interface{}, error) {
	id, err := myhttp.FindIdVariable(r)
	if err != nil {
		return nil, err
	}

	return deleteProductRequest{ProductId(id)}, nil
}

func decodeGetReviewsRequest(_ context.Context, r *http.Request) (interface{}, error) {
	id, err := myhttp.FindIdVariable(r)
	if err != nil {
		return nil, err
	}

	return getReviewsRequest{Id: ProductId(id)}, err
}

func decodeCreateReviewRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var review *ReviewCreationDTO
	if err := json.NewDecoder(r.Body).Decode(&review); err != nil {
		return nil, err
	}

	productId, err := myhttp.FindIdVariable(r)
	if err != nil {
		return nil, err
	}

	return createReviewRequest{ProductId: ProductId(productId), Review: review}, nil
}

func MakeHandler(r *mux.Router, service Service) {
	getProductByIdHandler := kithttp.NewServer(
		makeGetProductByIdEndpoint(service),
		decodeGetProductByIdRequest,
		kithttp.EncodeJSONResponse,
	)

	getAllProductsHandler := kithttp.NewServer(
		makeGetAllProductsEndpoint(service),
		decodeGetAllProducts,
		kithttp.EncodeJSONResponse,
	)

	createProductHandler := kithttp.NewServer(
		makeCreateProductEndpoint(service),
		decodeCreateProductRequest,
		kithttp.EncodeJSONResponse,
	)

	updateProductHandler := kithttp.NewServer(
		makeUpdateProductEndpoint(service),
		decodeUpdateProductRequest,
		kithttp.EncodeJSONResponse,
	)

	deleteProductHandler := kithttp.NewServer(
		makeDeleteProductEndpoint(service),
		decodeDeleteProductRequest,
		kithttp.EncodeJSONResponse,
	)

	getReviewsHandler := kithttp.NewServer(
		makeGetReviewsEndpoint(service),
		decodeGetReviewsRequest,
		kithttp.EncodeJSONResponse,
	)

	createReviewHandler := kithttp.NewServer(
		makeCreateReviewEndpoint(service),
		decodeCreateReviewRequest,
		kithttp.EncodeJSONResponse,
	)

	r.Handle("/products/{id}", getProductByIdHandler).Methods("GET")
	r.Handle("/products", getAllProductsHandler).Methods("GET")
	r.Handle("/products", createProductHandler).Methods("POST")
	r.Handle("/products/{id}", updateProductHandler).Methods("PUT")
	r.Handle("/products/{id}", deleteProductHandler).Methods("DELETE")
	r.Handle("/products/{id}/reviews", getReviewsHandler).Methods("GET")
	r.Handle("/products/{id}/reviews", createReviewHandler).Methods("POST")

}
