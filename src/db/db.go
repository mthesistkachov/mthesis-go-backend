package db

import (
	"database/sql"
	"fmt"
	"os"

	"mthesis/src/config"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func New(config *config.Config) (*gorm.DB, error) {
	conn, err := sql.Open("pgx", config.GetDBConnStr())
	gormDB, err := gorm.Open(postgres.New(postgres.Config{Conn: conn}))
	if err != nil {
		// TODO proper logging
		fmt.Fprintf(os.Stderr, "Unable to connect to database: %v\n", err)
		return nil, err
	}

	//gormDB.AutoMigrate(&model.Product{})

	// var products []*model.Product
	// gormDB.Find(&products)
	// for _, product := range products {
	// 	fmt.Printf("%+v\n", product)
	// }

	return gormDB, nil
}
