package log

import (
	kitlog "github.com/go-kit/log"
	"io"
	"os"
)

func cliWriter() (io.Writer, error) {
	return kitlog.NewSyncWriter(os.Stdout), nil
}

func cliAndFileWriter() (io.Writer, error) {
	opt := os.O_APPEND | os.O_CREATE | os.O_WRONLY
	f, err := os.OpenFile("mthesis.log", opt, 0644)
	if err != nil {
		return nil, err
	}

	writer := io.MultiWriter(os.Stdout, f)
	writer = kitlog.NewSyncWriter(writer)

	return writer, nil
}

func Init(logToFile bool) (kitlog.Logger, error) {
	writerConstructor := cliWriter
	if logToFile {
		writerConstructor = cliAndFileWriter
	}

	writer, err := writerConstructor()
	if err != nil {
		return nil, err
	}

	logger := kitlog.NewLogfmtLogger(writer)
	logger = kitlog.With(logger, "ts", kitlog.DefaultTimestampUTC, "caller", kitlog.DefaultCaller)
	//stdlog.SetOutput(kitlog.NewStdlibAdapter(logger))

	return logger, nil
}
