
CREATE TABLE products
(
    id           SERIAL PRIMARY KEY,
    name         TEXT,
    description  TEXT,
    price        NUMERIC(10, 2),
    is_available BOOLEAN
);

CREATE TABLE reviews
(
    id      SERIAL PRIMARY KEY,
    title   TEXT,
    product_id INT REFERENCES products(id),
    content TEXT,
    rating  INT,
    user_id TEXT
);
