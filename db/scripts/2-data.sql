INSERT INTO products (name, description, price, is_available) VALUES ('Ball', 'Just a ball', 2.50, true);
INSERT INTO products (name, description, price, is_available) VALUES ('Wine', 'The cheap one', 4.00, false);

INSERT INTO reviews (product_id, title, content, rating, user_id) VALUES (1, 'Perfect', 'Cool', 5, '12afc1f2-3610-4a3a-a0e1-8530df854ab7');
INSERT INTO reviews (product_id, title, content, rating, user_id) VALUES (1, 'Disappointing', 'Lame', 2, '470bc152-9bc6-40ed-a44f-fed0ee4c3d4e');
