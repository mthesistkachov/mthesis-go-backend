#!/bin/bash

docker-compose rm -f
docker-compose up \
    --build \
    --force-recreate \
    --always-recreate-deps \
    --remove-orphans \
