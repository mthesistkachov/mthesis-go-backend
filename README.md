# Mthesis Go Backend

## Prerequisites

* Install docker and docker compose
* Clone the repositories of the backends, the frontend and the kafka-echo-service into the same folder. Do not change their names.
* Add this to your local hosts file:

```
127.0.0.1	mthesis.com
127.0.0.1	api.mthesis.com
```

## How to run

Execute

```
bash composeUp.sh
```

and visit http://mthesis.com in a web browser after everything is up and online.

Source code is mounted into the containers and the applications are restarted when a change is detected.
