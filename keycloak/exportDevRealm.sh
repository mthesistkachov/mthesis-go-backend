#!/bin/bash

# Instructions:
# Start the keycloak container with docker compose and let it import the realm
# Make your changes
# Run this script. Once the extra admin console started press Ctrl+C to exit it
# The realm will be copied out of the container after exiting
# Restart docker compose

docker exec -it mthesis-go-backend_keycloak_1 /opt/jboss/keycloak/bin/standalone.sh \
  -Djboss.socket.binding.port-offset=100 \
  -Dkeycloak.migration.action=export \
  -Dkeycloak.migration.provider=singleFile \
  -Dkeycloak.migration.realmName=dev \
  -Dkeycloak.migration.usersExportStrategy=REALM_FILE \
  -Dkeycloak.migration.file=/tmp/dev-realm-export.json

docker cp mthesis-go-backend_keycloak_1:/tmp/dev-realm-export.json ./dev-realm.json
echo Realm exported

#docker exec -it mthesis-go-backend_keycloak_1 ls -l /tmp/
