FROM golang:1.17.5-bullseye AS builder

RUN useradd -ms /bin/bash go
USER go
RUN mkdir -p /home/go/app

WORKDIR /home/go/app

COPY ./go.mod ./go.sum ./
RUN go mod download

COPY . .

RUN go build src/main.go

FROM debian:11.2-slim

COPY --from=builder /home/go/app/main ./main
EXPOSE 8080
CMD ["./main"]
