module mthesis

go 1.16

require (
	github.com/Nerzal/gocloak/v10 v10.0.1 // indirect
	github.com/go-kit/kit v0.12.0
	github.com/go-kit/log v0.2.0
	github.com/go-resty/resty/v2 v2.7.0 // indirect
	github.com/golang-jwt/jwt/v4 v4.3.0 // indirect
	github.com/google/uuid v1.3.0
	github.com/gorilla/mux v1.8.0
	github.com/jackc/pgx/v4 v4.15.0 // indirect
	github.com/klauspost/compress v1.15.2 // indirect
	github.com/prometheus/client_golang v1.12.1
	github.com/segmentio/kafka-go v0.4.31 // indirect
	golang.org/x/crypto v0.0.0-20220131195533-30dcbda58838 // indirect
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df // indirect
	gorm.io/driver/postgres v1.2.3
	gorm.io/gorm v1.22.5
)
