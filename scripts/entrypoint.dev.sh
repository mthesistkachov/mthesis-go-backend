#!/bin/bash

set -e

CompileDaemon \
    --build="bash ./scripts/build.sh" \
    --command="bash ./scripts/startdebugger.sh"

